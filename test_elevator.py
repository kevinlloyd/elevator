import unittest
from elevator_manager import *

class TestElevator(unittest.TestCase):

    def setUp(self):
        pass

    def test_canInitElevator(self):
        e = Elevator('Test Elevator', 7)
        self.assertEquals(7, e.maxFloors)

        manager = ElevatorManager(10, 4)
        self.assertEquals(10, manager.maxFloors)
        self.assertEquals(4, manager.numElevators)
        self.assertEquals(4, len(manager.elevators))

        self.assertTrue(True)

    def test_moveElevator(self):
        """
        Simple tests that move the elevator from one floor to the next based on what floors have been queued up
        """
        e = Elevator('Move Test', 10)

        e.gotoFloor(4)
        self.assertEquals(1, e.currentFloor)

        e.moveOne()
        self.assertEquals(2, e.currentFloor)

        e.moveOne()
        self.assertEquals(3, e.currentFloor)

        e.moveOne()
        self.assertEquals(4, e.currentFloor)
        self.assertEquals(0, len(e.destinationFloors)) # ensure destination cleared when arrived

        e.moveOne()
        self.assertEquals(4, e.currentFloor) # cannot pass

    def test_smartQueingUp(self):
        """
        Testing destination queue scenario: start on floor 10, user selects floor 5. Get to floor 6,
        users select floor 3 and 7.
        Since elevator currently moving down, should queue 5, 3, 7
        """
        e = Elevator('Queue Up', 10)
        e.gotoFloor(5)
        e.move(3)

        # While on the 4th floor going up to 5th, someone selects 2 (back down), then 7 (up)
        self.assertEquals(4, e.currentFloor)
        e.gotoFloor(3)

        self.assertEquals(0, e.destinationFloors.index(5))
        self.assertEquals(1, e.destinationFloors.index(3))

        e.gotoFloor(7)

        # Will serve 5 then 7 since it's already moving up.
        self.assertEquals(0, e.destinationFloors.index(5))
        self.assertEquals(1, e.destinationFloors.index(7))
        self.assertEquals(2, e.destinationFloors.index(3))

    def test_smartQueingDown(self):
        """
        Testing destination queue scenario: start on floor 1, user selects floor 5. Get to floor 6,
        users select floor 3 and 7.
        Since elevator currently moving up, should queue 5, 7, 3
        """
        e = Elevator('Queue Down', 10)
        e.currentFloor = 10 # set top floor
        e.gotoFloor(5)
        e.move(4)

        # While on the 4th floor going up to 5th, someone selects 2 (back down), then 7 (up)
        self.assertEquals(6, e.currentFloor)
        e.gotoFloor(3)

        self.assertEquals(0, e.destinationFloors.index(5))
        self.assertEquals(1, e.destinationFloors.index(3))

        e.gotoFloor(7)

        # Will serve 5 then 7 since it's already moving down.
        self.assertEquals(0, e.destinationFloors.index(5))
        self.assertEquals(1, e.destinationFloors.index(3))
        self.assertEquals(2, e.destinationFloors.index(7))

    def test_getDistanceNotMoving(self):
        e = Elevator('Distance', 10)
        e.currentFloor = 2
        d = e.getDistance(5, 'down')
        self.assertEquals(5-2, d)

        d = e.getDistance(5, 'up')
        self.assertEquals(5-2, d)

    def test_getDistanceAlongPath(self):
        e = Elevator('Distance', 10)
        e.currentFloor = 9
        e.gotoFloor(4)

        d = e.getDistance(5, 'down')
        self.assertEquals(9-5, d)

    def test_getDistancePastCurrentDown(self):
        e = Elevator('Distance', 10)
        e.currentFloor = 4
        e.gotoFloor(1)

        d = e.getDistance(5, 'down')
        self.assertEquals(4-1 + 5-1, d) # travel down and back to 5

        e.gotoFloor(4)

        d = e.getDistance(5, 'down')
        # 4-1 + 4-1 + 5-4
        self.assertEquals(7, d) # travel down, then to 4, then back to 5

        e.gotoFloor(6)
        d = e.getDistance(5, 'down')
        # 4-1 + 6-1 + 6-5
        self.assertEquals(9, d) # travel down, then to 4, then back to 5

    def test_getDistancePastCurrentUp(self):
        e = Elevator('Distance', 10)
        e.currentFloor = 6
        e.gotoFloor(10)

        # print e.destinationFloors

        # d = e.getDistance(5, 'up')
        # self.assertEquals(10-6 + 10-5, d) # travel down and back to 5

        e.gotoFloor(7)
        print e.destinationFloors

        d = e.getDistance(5, 'up')
        self.assertEquals(10-6 + 10-7 + 7-5, d)

        e.gotoFloor(3)
        d = e.getDistance(5, 'up')
        self.assertEquals(10-6 + 10-3 + 5-3, d)


    def test_callElevator(self):
        manager = ElevatorManager(10, 4) # initialize bank of 4 elevators
        e1 = manager.elevators[0]
        e2 = manager.elevators[1]
        e3 = manager.elevators[2]
        e4 = manager.elevators[3]

        e1.currentFloor = 3
        e2.currentFloor = 2
        e3.currentFloor = 3
        e4.currentFloor = 4

        a = manager.getAvailableElevator(2, 'up')

        # already on floor 2
        self.assertEquals(a, e2)

    def test_callElevatorUp(self):
        """Calling an elevator from outside, assuming none are in the path"""
        manager = ElevatorManager(13, 4) # initialize bank of 4 elevators
        e1 = manager.elevators[0]
        e2 = manager.elevators[1]
        e3 = manager.elevators[2]
        e4 = manager.elevators[3]

        e1.currentFloor = 7
        e2.currentFloor = 7
        e3.currentFloor = 7
        e4.currentFloor = 7

        # Going all the way down
        e1.gotoFloor(1)
        e2.gotoFloor(2)
        e3.gotoFloor(3)
        e4.gotoFloor(4)
        

        self.assertEquals('down', e1.direction)
        self.assertEquals('down', e2.direction)
        self.assertEquals('down', e3.direction)
        self.assertEquals('down', e4.direction)

        
        a = manager.getAvailableElevator(8, 'down')

        # self.assertEquals(a, e4)


if __name__ == '__main__':
    unittest.main()