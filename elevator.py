class Elevator():
    """
    Represents and Elevator class. Controls all the internals of the Elevator
    """

    def __init__(self, name, maxFloors):
        self.name = name
        self.maxFloors = maxFloors
        self.currentFloor = 1
        self.destinationFloors = []
        self.direction = ''
        """List of destinations the elevator must fulfill"""

    def __repr__(self):
        return '{0}: Current Floor: {1} Direction: {2} Destinations: {3}'.format(
        	self.name, self.currentFloor, self.direction, self.destinationFloors)

    def isAvailable(self):
        """
        Returns true if there are not destinations to supply
        """
        return len(self.destinationFloors) != 0

    def gotoFloor(self, floorNumber):
        if floorNumber > self.maxFloors:
            raise Exception('Cannot go to floor higher than max')

        if floorNumber in self.destinationFloors:
            print '{0}: floor {1} already selected'.format(self.name, floorNumber)
            return

        self.destinationFloors.append(floorNumber)

        listOne = [] # Used for current direction
        listTwo = [] # Used for second direction
        nextFloor = self.destinationFloors[0]

        # going up
        if self.currentFloor < nextFloor:
            self.direction = 'up'
            for destination in sorted(self.destinationFloors):
                if (destination >= self.currentFloor):
                    listOne.append(destination) # list going up
                else:
                    listTwo.append(destination)
            listTwo.reverse()
        # going down
        else:
            self.direction = 'down'
            for destination in reversed(sorted(self.destinationFloors)):
                if (destination <= self.currentFloor):
                    listOne.append(destination) # list going up
                else:
                    listTwo.append(destination)
            listTwo.reverse()

        self.destinationFloors = listOne + listTwo

    def move(self, numberOfFloors):
        for i in xrange(0, numberOfFloors):
            self.moveOne()

    def moveOne(self):
        """
        Simulates moving the elevator one move forward. Will move one floor in the direction of the
        next queued destination
        """
        if len(self.destinationFloors) == 0:
            return

        nextFloor = self.destinationFloors[0]
        if self.currentFloor > nextFloor:
            self.currentFloor -= 1
        elif self.currentFloor < nextFloor:
            self.currentFloor += 1
        else:
            print 'No move need for ' + self.name

        if self.currentFloor == nextFloor:
            self.destinationFloors.remove(nextFloor)
            self.direction = ''
        # print '{0}. Current floor: {1}'.format(self.name, self.currentFloor)

    def getDistance(self, floor, direction):
        if self.direction == '':
            return abs(self.currentFloor - floor) # distance to get back

        maxFloor = max(self.destinationFloors)
        minFloor = min(self.destinationFloors)
        if direction == 'down':
            if self.currentFloor >= floor:
                return abs(self.currentFloor - floor)
            else:
                return abs(self.currentFloor - minFloor) + (maxFloor - minFloor) + abs(floor - maxFloor) 
        elif direction == 'up':
            if self.currentFloor <= floor:
                return abs(self.currentFloor - floor)
            else:
                return abs(self.currentFloor - maxFloor) + (maxFloor - minFloor) + abs(minFloor - floor)
        return self.maxFloors