module.exports = function(grunt) {
  grunt.initConfig({
    shell: {
      test: { 
        command: 'python test_elevator.py'
      },
      clear: { 
        command: 'clear'
      },
    },

    watch: {
      tests: {
        files: ['**/*.py'],
        tasks: ['shell:clear', 'shell:test']
      }
    },
  });

  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
};