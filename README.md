# Elevator System
This application will model an elevator system within a building

Elevators will travel up and down to different floors in the building. From within the elevator users can select which floors to go to by pushing the number of the floor. An elevator can queue up multiple directions if multiple users select different floors.

## Multiple Requests
Elevators moving in a particular path will need to fulfill all requests in the particular direction before switching directions.

### Example
An elevator on the 5th floor going down, will need to serve all requests for floors 1 through 4, before service requests for floors above 5. This is regardless of the order of the requests.

## External Requests
Elevators also need the ability to be called to different floors from the outside.

### Example
A user on the 5th floor, needs to get to the ground floor. The system should determine which elevator is best suited to serve his request. This to consider include:
- Available elevators
- Load of elevators
- Direction of elevators
- Queue for elevators