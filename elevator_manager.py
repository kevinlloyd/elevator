from elevator import *

class ElevatorManager():
    """
    Used to manager a suite of elevators in a building
    """

    def __init__(self, maxFloors, numElevators):
        self.maxFloors = maxFloors
        self.numElevators = numElevators
        self.elevators = []

        for i in xrange(0, numElevators):
            self.elevators.append(Elevator('Elevator {0}'.format(i+1) , maxFloors))

    def moveOne(self):
        """
        Simulates one move for all elevators
        """
        for e in ele:
            e.moveOne()

    def getAvailableElevator(self, floor, direction):
        closestElevator = -1
        
        shortestDistance = self.maxFloors + 1 # initialize shortest distance

        for e in self.elevators:
            if e.currentFloor == floor:
                return e
            if e.direction == direction or e.direction == '': # evaluate elevators
                distance = abs(e.currentFloor - floor)
                if distance < shortestDistance:
                    shortestDistance = distance
                    elevator = e
        return elevator

    def callElevator(self, floor, direction):
        e = getAvailableElevator(floor, direction)
        e.gotoFloor(floor)

